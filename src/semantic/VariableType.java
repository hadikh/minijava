package semantic;

/**
 * Type of variable
 */
public enum VariableType {
    INT, BOOLEAN, VOID;
}
