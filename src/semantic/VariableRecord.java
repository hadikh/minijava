package semantic;

import scan.Token;

/**
 * VariableRecord is used to record necessary data of a variable (field, local, or temporary)
 */
public class VariableRecord {
    public VariableType type;
    public String name;
    public Token token;
    public int address;

    public VariableRecord(VariableType type, String name, Token token, int address) {
        this.type = type;
        this.name = name;
        this.token = token;
        this.address = address;
    }

    @Override
    public String toString() {
        return String.format("VC{ %s:%s:%d }", token == null ? null : token.getValue(), type, address);
    }
}
