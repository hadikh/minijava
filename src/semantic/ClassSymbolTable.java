package semantic;

import scan.Token;

import java.util.Collection;
import java.util.HashMap;

/**
 * Class level symbol table
 */
public class ClassSymbolTable implements SymbolTable {
    private String name;
    private Token token;
    private HashMap<String, VariableRecord> fieldRecords = new HashMap<>();
    private HashMap<String, MethodSymbolTable> methodRecords = new HashMap<>();

    public ClassSymbolTable(Token token, String name) {
        this.token = token;
        this.name = name;
    }

    public Token getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public VariableRecord getField(String name) {
        return fieldRecords.get(name);
    }

    public MethodSymbolTable getMethod(String name) {
        return methodRecords.get(name);
    }

    public void addField(VariableRecord var) {
        fieldRecords.put(var.name, var);
    }

    public void addMethod(MethodSymbolTable mst) {
        methodRecords.put(mst.getName(), mst);
    }

    public Collection<VariableRecord> getFields() {
        return fieldRecords.values();
    }

    public Collection<MethodSymbolTable> getMethods() {
        return methodRecords.values();
    }

    @Override
    public String toString() {
        return String.format("CST{ name=%s, fields=%s, methods=%s }", name, getFields(), getMethods());
    }
}
