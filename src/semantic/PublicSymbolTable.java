package semantic;

import java.util.HashMap;

/**
 * Public symbol table
 */
public class PublicSymbolTable implements SymbolTable {
    private HashMap<String, ClassSymbolTable> classRecords = new HashMap<>();

    public ClassSymbolTable getClass(String name) {
        return classRecords.get(name);
    }

    public void addClass(ClassSymbolTable cst) {
        classRecords.put(cst.getName(), cst);
    }

    @Override
    public String toString() {
        return String.format("PST{ classes=%s }", classRecords.values().stream().map(ClassSymbolTable::getName).toArray());
    }
}
