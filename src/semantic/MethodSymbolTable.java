package semantic;

import scan.Token;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Method level symbol table
 */
public class MethodSymbolTable implements SymbolTable {
    private String name;
    private Token token;
    private int startAddress, returnAddress;
    private VariableRecord returnValue;
    private LinkedList<VariableRecord> parameters = new LinkedList<>();
    private final HashMap<String, VariableRecord> variableRecords;

    public MethodSymbolTable(Token token, String name, VariableRecord returnValue, int returnAddress, int startAddress) {
        this.token = token;
        this.name = name;
        this.returnValue = returnValue;
        this.returnAddress = returnAddress;
        this.startAddress = startAddress;
        variableRecords = new HashMap<>();
    }

    public MethodSymbolTable(MethodSymbolTable mst, String newName) {
        name = newName;
        token = mst.token;
        startAddress = mst.startAddress;
        returnAddress = mst.returnAddress;
        returnValue = mst.returnValue;
        parameters = mst.parameters;
        variableRecords = mst.variableRecords;
    }

    public Token getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public VariableRecord getReturnValue() {
        return returnValue;
    }

    public int getStartAddress() {
        return startAddress;
    }

    public int getReturnAddress() {
        return returnAddress;
    }

    public void addParameter(VariableRecord parameter) {
        parameters.add(parameter);
        variableRecords.put(parameter.name, parameter);
    }

    public LinkedList<VariableRecord> getParameters() {
        return parameters;
    }

    public VariableRecord getVariable(String name) {
        return variableRecords.get(name);
    }

    public void addVariable(VariableRecord var) {
        variableRecords.put(var.name, var);
    }

    @Override
    public String toString() {
        return String.format("MST{ name=%s, vars=%s }", name, variableRecords.values());
    }
}
