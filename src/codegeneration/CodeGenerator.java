package codegeneration;

import scan.Token;
import semantic.SymbolTable;

import java.util.Stack;

/**
 * Code generator interface
 */
public class CodeGenerator {
    public int index = 0;
    public final ProgramBlock programBlock = new ProgramBlock();
    public final Stack<Object> semanticStack = new Stack<>();
    public final Stack<SymbolTable> scopeStack = new Stack<>();
    public Token lookAhead;
    private int tempVarIndex = 2000;

    public int getTemp() {
        return tempVarIndex += 4;
    }

    @Override
    public String toString() {
        return String.format(
                "CG{ i=%d, pbl=%s, sem=%s, sym=%d, la=%s }",
                index, index > 0 ? programBlock.get(index-1) : null, semanticStack, scopeStack.size(), lookAhead
        );
    }
}
