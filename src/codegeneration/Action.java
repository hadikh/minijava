package codegeneration;

import lang.Symbol;
import scan.Token;
import semantic.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Actions for code generation
 */
public enum Action implements Symbol {
    //================//
    // useful actions //
    //================//

    label(g -> {
        g.semanticStack.push(g.index);
    }),

    save(g -> {
        g.semanticStack.push(g.index);
        g.programBlock.put(null);
        g.index++;
    }),

    pushNext(g -> {
        g.semanticStack.push(g.lookAhead);
    }),

    dup(g -> {
        g.semanticStack.push(g.semanticStack.peek());
    }),

    jumpNext(g -> {
        g.programBlock.put(new Instruction(InstructionType.JP, g.index + 2));
        g.index++;
    }),

    jumpHere(g -> {
        int savedAddress = (int) g.semanticStack.pop();
        g.programBlock.set(savedAddress, new Instruction(InstructionType.JP, g.index));
    }),


    //==============================//
    // actions for class generation //
    //==============================//

    pushPublicScope(g -> {
        g.scopeStack.push(new PublicSymbolTable());
    }),

    popScope(g -> {
        g.scopeStack.pop();
    }),

    pushClassScope(g -> {
        PublicSymbolTable pst = (PublicSymbolTable) g.scopeStack.peek();
        Token identifier = (Token) g.semanticStack.pop();
        String name = identifier.getValue();
        if (pst.getClass(name) != null) {
            name = findEmpty(pst::getClass, identifier.getValue());
            err("Class already exists", identifier);
        }
        ClassSymbolTable cst = new ClassSymbolTable(identifier, name);
        pst.addClass(cst);
        g.scopeStack.push(cst);
    }),

    extend(g -> {
        PublicSymbolTable pst = (PublicSymbolTable) g.scopeStack.get(0);
        Token identifier = (Token) g.semanticStack.pop();
        ClassSymbolTable cst = (ClassSymbolTable) g.scopeStack.peek();
        ClassSymbolTable cstParent = pst.getClass(identifier.getValue());
        if (cstParent == null) {
            err("Parent class doest not exists", identifier);
        } else {
            // extend fields
            for (VariableRecord var : cstParent.getFields()) {
                String name = findExtendedName(cst::getField, var.name);
                if (name != null)
                    cst.addField(new VariableRecord(var.type, name, var.token, var.address));
            }
            // extend methods
            for (MethodSymbolTable mst : cstParent.getMethods()) {
                String name = findExtendedName(cst::getMethod, mst.getName());
                if (name != null)
                    cst.addMethod(new MethodSymbolTable(mst, name));
            }
        }
    }),

    addField(g -> {
        ClassSymbolTable cst = (ClassSymbolTable) g.scopeStack.peek();
        Token identifier = (Token) g.semanticStack.pop();
        Token type = (Token) g.semanticStack.pop();
        String name = identifier.getValue();
        if (cst.getField(name) != null) {
            name = findEmpty(cst::getField, identifier.getValue());
            err("Field " + identifier.getValue() + " already exists", identifier);
        }
        VariableRecord field = new VariableRecord(VariableType.valueOf(type.getValue().toUpperCase()), name, identifier, g.getTemp());
        cst.addField(field);
    }),

    pushMethodScope(g -> {
        ClassSymbolTable cst = (ClassSymbolTable) g.scopeStack.peek();
        Token identifier = (Token) g.semanticStack.pop();
        Token type = (Token) g.semanticStack.pop();
        VariableRecord ret = new VariableRecord(VariableType.valueOf(type.getValue().toUpperCase()), null, type, g.getTemp());
        String name = identifier.getValue();
        if (cst.getMethod(name) != null) {
            name = findEmpty(cst::getMethod, name);
            err("Method already exists", identifier);
        }
        MethodSymbolTable mst = new MethodSymbolTable(identifier, name, ret, g.getTemp(), g.index);
        cst.addMethod(mst);
        g.scopeStack.push(mst);
    }),

    addParameter(g -> {
        MethodSymbolTable mst = (MethodSymbolTable) g.scopeStack.peek();
        Token identifier = (Token) g.semanticStack.pop();
        Token type = (Token) g.semanticStack.pop();
        String name = identifier.getValue();
        if (mst.getVariable(name) != null) {
            name = findEmpty(mst::getVariable, name);
            err("Variable already exists in the scope", identifier);
        }
        VariableRecord var = new VariableRecord(VariableType.valueOf(type.getValue().toUpperCase()), name, identifier, g.getTemp());
        mst.addParameter(var);
    }),

    addVar(g -> {
        MethodSymbolTable mst = (MethodSymbolTable) g.scopeStack.peek();
        Token identifier = (Token) g.semanticStack.pop();
        Token type = (Token) g.semanticStack.pop();
        String name = identifier.getValue();
        if (mst.getVariable(name) != null) {
            name = findEmpty(mst::getVariable, name);
            err("Variable already exists in the scope", identifier);
        }
        VariableRecord var = new VariableRecord(VariableType.valueOf(type.getValue().toUpperCase()), name, identifier, g.getTemp());
        mst.addVariable(var);
    }),

    doReturn(g -> {
        MethodSymbolTable mst = (MethodSymbolTable) g.scopeStack.peek();
        VariableRecord expr = (VariableRecord) g.semanticStack.pop();
        VariableRecord ret = mst.getReturnValue();
        if (expr.type != ret.type) {
            err("Return type does not match with the returned value", expr.token);
        }
        g.programBlock.put(new Instruction(InstructionType.ASSIGN, expr.address, ret.address));
        g.index++;
        g.programBlock.put(new Instruction(InstructionType.JP, ind(mst.getReturnAddress())));
        g.index++;
    }),


    //=============================//
    // assignment and computation  //
    //=============================//

    assign(g -> {
        VariableRecord o2 = (VariableRecord) g.semanticStack.pop();
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != o2.type) {
            err("Can not assign a variable to a different type", o1.token);
        }
        g.programBlock.put(new Instruction(InstructionType.ASSIGN, o2.address, o1.address));
        g.index++;
    }),

    add(g -> {
        VariableRecord o2 = (VariableRecord) g.semanticStack.pop();
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != VariableType.INT) {
            err("First operand should be an integer", o1.token);
        }
        if (o2.type != VariableType.INT) {
            err("Second operand should be an integer", o2.token);
        }
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.INT, null, o1.token, t));
        g.programBlock.put(new Instruction(InstructionType.ADD, o1.address, o2.address, t));
        g.index++;
    }),

    minus(g -> {
        VariableRecord o2 = (VariableRecord) g.semanticStack.pop();
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != VariableType.INT) {
            err("First operand should be an integer", o1.token);
        }
        if (o2.type != VariableType.INT) {
            err("Second operand should be an integer", o2.token);
        }
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.INT, null, o1.token, t));
        g.programBlock.put(new Instruction(InstructionType.SUB, o1.address, o2.address, t));
        g.index++;
    }),

    mult(g -> {
        VariableRecord o2 = (VariableRecord) g.semanticStack.pop();
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != VariableType.INT) {
            err("First operand should be an integer", o1.token);
        }
        if (o2.type != VariableType.INT) {
            err("Second operand should be an integer", o2.token);
        }
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.INT, null, o1.token, t));
        g.programBlock.put(new Instruction(InstructionType.MULT, o1.address, o2.address, t));
        g.index++;
    }),

    and(g -> {
        VariableRecord o2 = (VariableRecord) g.semanticStack.pop();
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != VariableType.BOOLEAN) {
            err("First operand should be an integer", o1.token);
        }
        if (o2.type != VariableType.BOOLEAN) {
            err("Second operand should be an integer", o2.token);
        }
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.BOOLEAN, null, o1.token, t));
        g.programBlock.put(new Instruction(InstructionType.AND, o1.address, o2.address, t));
        g.index++;
    }),

    equals(g -> {
        VariableRecord o2 = (VariableRecord) g.semanticStack.pop();
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != o2.type) {
            err("First and second operands should be from the same type", o1.token);
        }
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.BOOLEAN, null, o1.token, t));
        g.programBlock.put(new Instruction(InstructionType.EQ, o1.address, o2.address, t));
        g.index++;
    }),

    lessThan(g -> {
        VariableRecord o2 = (VariableRecord) g.semanticStack.pop();
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != o2.type) {
            err("First and second operands should be from the same type", o1.token);
            return;
        }
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.BOOLEAN, null, o1.token, t));
        g.programBlock.put(new Instruction(InstructionType.LT, o1.address, o2.address, t));
        g.index++;
    }),

    pushInteger(g -> {
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.INT, null, g.lookAhead, t));
        g.programBlock.put(new Instruction(InstructionType.ASSIGN, imm(Integer.parseInt(g.lookAhead.getValue())), t));
        g.index++;
    }),

    pushTrue(g -> {
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.BOOLEAN, null, g.lookAhead, t));
        g.programBlock.put(new Instruction(InstructionType.ASSIGN, imm(1), t));
        g.index++;
    }),

    pushFalse(g -> {
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(VariableType.BOOLEAN, null, g.lookAhead, t));
        g.programBlock.put(new Instruction(InstructionType.ASSIGN, imm(0), t));
        g.index++;
    }),


    //===================================//
    // referencing fields and local vars //
    //===================================//

    varRef(g -> {
        Token identifier = (Token) g.semanticStack.pop();
        MethodSymbolTable mst = (MethodSymbolTable) g.scopeStack.peek();
        VariableRecord var = mst.getVariable(identifier.getValue());
        if (var == null) {
            ClassSymbolTable cst = (ClassSymbolTable) g.scopeStack.get(1);
            var = cst.getField(identifier.getValue());
            if (var == null) {
                var = cst.getField("p$" + identifier.getValue());
                if (var == null) {
                    err("Identifier " + identifier.getValue() + " is not defined", identifier);
                    var = new VariableRecord(VariableType.INT, identifier.getValue(), identifier, g.getTemp());
                    mst.addVariable(var);
                }
            }
        }
        g.semanticStack.push(var);
    }),

    fieldRef(g -> {
        Token fieldName = (Token) g.semanticStack.pop();
        Token className = (Token) g.semanticStack.pop();
        PublicSymbolTable pst = (PublicSymbolTable) g.scopeStack.get(0);
        ClassSymbolTable cst = pst.getClass(className.getValue());
        VariableRecord var = cst.getField(fieldName.getValue());
        if (var == null) {
            var = cst.getField("p$" + fieldName.getValue());
            if (var == null) {
                err("Field " + className.getValue() + "." + fieldName.getValue() + " not found", fieldName);
                var = new VariableRecord(VariableType.INT, fieldName.getValue(), fieldName, g.getTemp());
                cst.addField(var);
            }
        }
        g.semanticStack.push(var);
    }),


    //=============================//
    //   method reference & call   //
    //=============================//

    methodRef(g -> {
        Token methodName = (Token) g.semanticStack.pop();
        Token className = (Token) g.semanticStack.pop();
        PublicSymbolTable pst = (PublicSymbolTable) g.scopeStack.get(0);
        ClassSymbolTable cst = pst.getClass(className.getValue());
        MethodSymbolTable method = cst.getMethod(methodName.getValue());
        if (method == null) {
            method = cst.getMethod("p$" + methodName.getValue());
            if (method == null) {
                err("Method " + methodName.getValue() + " not found", methodName);
                method = new MethodSymbolTable(methodName, methodName.getValue(), new VariableRecord(VariableType.INT, null, g.lookAhead, g.getTemp()), g.getTemp(), 0);
                cst.addMethod(method);
            }
        }
        g.semanticStack.push(method.getReturnValue());
        g.semanticStack.push(method.getStartAddress());
        g.semanticStack.push(method.getReturnAddress());
        LinkedList<VariableRecord> parameters = method.getParameters();
        Iterator<VariableRecord> it = parameters.descendingIterator();
        g.semanticStack.push(null);
        while (it.hasNext())
            g.semanticStack.push(it.next());
    }),

    methodArg(g -> {
        VariableRecord arg = (VariableRecord) g.semanticStack.pop();
        VariableRecord var = (VariableRecord) g.semanticStack.peek();
        if (var == null) {
            err("Too extra arguments passed to method", arg.token);
        } else {
            if (arg.type != var.type) {
                err("Argument type does not match with method header", arg.token);
            }
            g.semanticStack.pop();
            g.programBlock.put(new Instruction(InstructionType.ASSIGN, arg.address, var.address));
            g.index++;
        }
    }),

    methodCall(g -> {
        VariableRecord var = (VariableRecord) g.semanticStack.pop();
        if (var != null) {
            err("Too few arguments for method", g.lookAhead);
            while (g.semanticStack.pop() != null);
        }
        int returnAddress = (int) g.semanticStack.pop();
        g.programBlock.put(new Instruction(InstructionType.ASSIGN, imm(g.index + 2), returnAddress));
        g.index++;
        int startAddress = (int) g.semanticStack.pop();
        g.programBlock.put(new Instruction(InstructionType.JP, startAddress));
        g.index++;
        VariableRecord returnValue = (VariableRecord) g.semanticStack.pop();
        int t = g.getTemp();
        g.semanticStack.push(new VariableRecord(returnValue.type, null, g.lookAhead, t));
        g.programBlock.put(new Instruction(InstructionType.ASSIGN, returnValue.address, t));
        g.index++;
    }),


    //==============================//
    //   flow of control commands   //
    //==============================//

    endIf(g -> {
        Integer secondJumpLocation = (Integer) g.semanticStack.pop();
        Integer firstJumpLocation = (Integer) g.semanticStack.pop();
        VariableRecord condition = (VariableRecord) g.semanticStack.pop();
        if (condition.type != VariableType.BOOLEAN) {
            err("Condition inside 'if' must be a boolean expression", condition.token);
        }
        g.programBlock.set(firstJumpLocation, new Instruction(InstructionType.JPF, condition.address, secondJumpLocation + 1));
        g.programBlock.set(secondJumpLocation, new Instruction(InstructionType.JP, g.index));
    }),


    endWhile(g -> {
        Integer jumpLocationAfterCondition = (Integer) g.semanticStack.pop();
        VariableRecord condition = (VariableRecord) g.semanticStack.pop();
        Integer labelBeforeWhile = (Integer) g.semanticStack.pop();
        if (condition.type != VariableType.BOOLEAN) {
            err("Condition inside 'while' must be a boolean expression", condition.token);
        }
        g.programBlock.set(jumpLocationAfterCondition, new Instruction(InstructionType.JPF, condition.address, g.index + 1));
        g.programBlock.put(new Instruction(InstructionType.JP, labelBeforeWhile));
        g.index++;
    }),

    endCase(g -> {
        int forwardJump = (int) g.semanticStack.pop();
        VariableRecord condition = (VariableRecord) g.semanticStack.pop();
        g.programBlock.set(forwardJump, new Instruction(InstructionType.JPF, condition.address, g.index + 1));
        g.programBlock.put(new Instruction(InstructionType.JP, (Integer) g.semanticStack.get(g.semanticStack.size() - 2)));
        g.index++;
    }),

    endSwitch(g -> {
        g.semanticStack.pop();
        g.programBlock.set((Integer) g.semanticStack.pop(), new Instruction(InstructionType.JP, g.index));
    }),


    //===================//
    //     built-ins     //
    //===================//

    print(g -> {
        VariableRecord o1 = (VariableRecord) g.semanticStack.pop();
        if (o1.type != VariableType.INT) {
            err("Type of output must be INTEGER", o1.token);
        }
        g.programBlock.put(new Instruction(InstructionType.PRINT, o1.address));
        g.index++;
    }),;

    private Consumer<CodeGenerator> action;

    private Action(Consumer<CodeGenerator> action) {
        this.action = action;
    }

    private static String imm(int value) {
        return "#" + value;
    }

    private static String ind(int address) {
        return "@" + address;
    }

    private static String findEmpty(Function<String, Object> finder, String prefix) {
        for (int i = 0; ; i++) {
            if (finder.apply(prefix + "$" + i) == null)
                return prefix + "$" + i;
        }
    }

    private static String findExtendedName(Function<String, Object> finder, String parentName) {
        String name = null;
        if (parentName.startsWith("p$")) {
            if (finder.apply(parentName) == null)
                name = parentName;
        } else {
            name = "p$" + parentName;
        }
        return name;
    }

    public static void err(String msg, Token token) {
        System.err.println("Semantic Error: " + msg + ", on line " + token.getLineNumber() + " column " + token.getColumnNumber() + ".");
    }

    public void run(CodeGenerator codeGenerator) {
        action.accept(codeGenerator);
    }
}
