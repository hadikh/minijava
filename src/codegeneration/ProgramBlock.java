package codegeneration;

import java.util.ArrayList;

public class ProgramBlock {
    private final ArrayList<Instruction> instructions = new ArrayList<>();

    public Instruction get(int index) {
        return instructions.get(index);
    }

    public Instruction[] getAll() {
        return instructions.toArray(new Instruction[instructions.size()]);
    }

    public void set(int index, Instruction instruction) {
        if (index >= instructions.size()) {
            throw new IndexOutOfBoundsException("Instruction gap detected.");
        } else {
            instructions.set(index, instruction);
        }
    }

    public void put(Instruction instruction) {
        instructions.add(instruction);
    }
}
