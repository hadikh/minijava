package codegeneration;

/**
 * Instruction types with their number of arguments
 */
public enum InstructionType {
    ADD(3), AND(3), ASSIGN(2), EQ(3), JPF(2), JP(1), LT(3), MULT(3), NOT(2), PRINT(1), SUB(3);

    private int argsLength;

    private InstructionType(int argLength) {
        this.argsLength = argLength;
    }

    public int getArgsLength() {
        return argsLength;
    }
}
