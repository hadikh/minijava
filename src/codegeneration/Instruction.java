package codegeneration;

/**
 * Instruction, consists of type and arguments
 */
public class Instruction {
    private InstructionType instructionType;
    private Object args[];

    public Instruction(InstructionType instructionType, Object... args) {
        this.instructionType = instructionType;
        this.args = args;
        if (args.length != instructionType.getArgsLength())
            throw new RuntimeException("Instruction " + instructionType + ", number of arguments is wrong.");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("(").append(instructionType);
        for (Object arg : args)
            sb.append(", ").append(arg);
        return sb.append(")").toString();
    }
}
