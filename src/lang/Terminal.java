package lang;

/**
 * Terminals of the grammar
 */
public enum Terminal implements Symbol {
    EOF("EOF"),
    PUBLIC("public"),
    CLASS("class"),
    LEFT_BRACE("{"),
    STATIC("static"),
    VOID("void"),
    MAIN("main"),
    LEFT_PAREN("("),
    RIGHT_PAREN(")"),
    RIGHT_BRACE("}"),
    EXTENDS("extends"),
    SEMICOLON(";"),
    RETURN("return"),
    COMMA(","),
    BOOLEAN("boolean"),
    INT("int"),
    IF("if"),
    ELSE("else"),
    WHILE("while"),
    SWITCH("switch"),
    SYSTEM_OUT("System.out.println"),
    ASSIGN("="),
    CASE("case"),
    COLON(":"),
    BREAK("break"),
    PLUS("+"),
    MINUS("-"),
    TIMES("*"),
    DOT("."),
    TRUE("true"),
    FALSE("false"),
    AND_AND("&&"),
    EQUALS("=="),
    LESS_THAN("<"),
    IDENTIFIER_LITERAL(null),
    INTEGER_LITERAL(null),
    ;

    private String keyword;

    private Terminal(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }
}
