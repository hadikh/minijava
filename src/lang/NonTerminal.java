package lang;

/**
 * Non-terminals of the grammar
 */
public enum NonTerminal implements Symbol {
    GOAL(""),
    SOURCE("Source could not be empty"),
    MAIN_CLASS("Source code must contain a public class"),
    CLASS_DECLARATIONS(""),
    CLASS_DECLARATION("Only last class is assumed to be public"),
    EXTENSION(""),
    FIELD_DECLARATIONS(""),
    FIELD_DECLARATION("Field declaration expected"),
    VAR_DECLARATIONS(""),
    VAR_DECLARATION("Variable declaration expected"),
    METHOD_DECLARATIONS(""),
    METHOD_DECLARATION("Method declaration expected"),
    PARAMETERS(""),
    PARAMETER(""),
    TYPE("Variable type expected"),
    STATEMENTS(""),
    STATEMENT("Statement expected"),
    CASE_STATEMENTS("Case statement expected"),
    CASE_STATEMENTS_REM(""),
    CASE_STATEMENT("Case statement expected"),
    GEN_EXPRESSION("Expression expected"),
    GEN_EXPRESSION_REM(""),
    EXPRESSION("Arithmetic expression expected"),
    EXPRESSION_REM(""),
    TERM("Term expected"),
    TERM_REM(""),
    FACTOR("Factor expected"),
    FACTOR_REM(""),
    FACTOR_REM_REM(""),
    REL_EXPRESSION_REM(""),
    REL_EXPRESSION_REM_REM(""),
    ARGUMENTS(""),
    ARGUMENT(""),
    IDENTIFIER("Identifier expected"),
    INTEGER("Number expected"),
    ;

    private String errorMessage;

    private NonTerminal(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMissingError() {
        return errorMessage;
    }
}
