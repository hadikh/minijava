package lang;

import codegeneration.Action;

/**
 * Rules of the grammar
 */
public enum Rule {
    R0(null, null), // sync sign
    R1(NonTerminal.GOAL,	new Symbol[]{Action.pushPublicScope, NonTerminal.SOURCE, Action.popScope, Terminal.EOF, }),
    R2(NonTerminal.SOURCE,	new Symbol[]{Action.save, NonTerminal.CLASS_DECLARATIONS, Action.jumpHere, NonTerminal.MAIN_CLASS, }),
    R3(NonTerminal.MAIN_CLASS,	new Symbol[]{Terminal.PUBLIC, Terminal.CLASS, NonTerminal.IDENTIFIER, Action.pushClassScope, Terminal.LEFT_BRACE, Terminal.PUBLIC, Terminal.STATIC, Action.pushNext, Terminal.VOID, Action.pushNext, Terminal.MAIN, Action.pushMethodScope, Terminal.LEFT_PAREN, Terminal.RIGHT_PAREN, Terminal.LEFT_BRACE, NonTerminal.VAR_DECLARATIONS, NonTerminal.STATEMENTS, Terminal.RIGHT_BRACE, Action.popScope, Terminal.RIGHT_BRACE, Action.popScope, }),
    R4(NonTerminal.CLASS_DECLARATIONS,	new Symbol[]{NonTerminal.CLASS_DECLARATION, NonTerminal.CLASS_DECLARATIONS, }),
    R5(NonTerminal.CLASS_DECLARATIONS,	new Symbol[]{}),
    R6(NonTerminal.CLASS_DECLARATION,	new Symbol[]{Terminal.CLASS, NonTerminal.IDENTIFIER, Action.pushClassScope, NonTerminal.EXTENSION, Terminal.LEFT_BRACE, NonTerminal.FIELD_DECLARATIONS, NonTerminal.METHOD_DECLARATIONS, Terminal.RIGHT_BRACE, Action.popScope, }),
    R7(NonTerminal.EXTENSION,	new Symbol[]{Terminal.EXTENDS, NonTerminal.IDENTIFIER, Action.extend, }),
    R8(NonTerminal.EXTENSION,	new Symbol[]{}),
    R9(NonTerminal.FIELD_DECLARATIONS,	new Symbol[]{NonTerminal.FIELD_DECLARATION, NonTerminal.FIELD_DECLARATIONS, }),
    R10(NonTerminal.FIELD_DECLARATIONS,	new Symbol[]{}),
    R11(NonTerminal.FIELD_DECLARATION,	new Symbol[]{Terminal.STATIC, NonTerminal.TYPE, NonTerminal.IDENTIFIER, Terminal.SEMICOLON, Action.addField, }),
    R12(NonTerminal.VAR_DECLARATIONS,	new Symbol[]{NonTerminal.VAR_DECLARATION, NonTerminal.VAR_DECLARATIONS, }),
    R13(NonTerminal.VAR_DECLARATIONS,	new Symbol[]{}),
    R14(NonTerminal.VAR_DECLARATION,	new Symbol[]{NonTerminal.TYPE, NonTerminal.IDENTIFIER, Action.addVar, Terminal.SEMICOLON, }),
    R15(NonTerminal.METHOD_DECLARATIONS,	new Symbol[]{NonTerminal.METHOD_DECLARATION, NonTerminal.METHOD_DECLARATIONS, }),
    R16(NonTerminal.METHOD_DECLARATIONS,	new Symbol[]{}),
    R17(NonTerminal.METHOD_DECLARATION,	new Symbol[]{Terminal.PUBLIC, Terminal.STATIC, NonTerminal.TYPE, NonTerminal.IDENTIFIER, Action.pushMethodScope, Terminal.LEFT_PAREN, NonTerminal.PARAMETERS, Terminal.RIGHT_PAREN, Terminal.LEFT_BRACE, NonTerminal.VAR_DECLARATIONS, NonTerminal.STATEMENTS, Terminal.RETURN, NonTerminal.GEN_EXPRESSION, Terminal.SEMICOLON, Action.doReturn, Terminal.RIGHT_BRACE, Action.popScope, }),
    R18(NonTerminal.PARAMETERS,	new Symbol[]{NonTerminal.TYPE, NonTerminal.IDENTIFIER, Action.addParameter, NonTerminal.PARAMETER, }),
    R19(NonTerminal.PARAMETERS,	new Symbol[]{}),
    R20(NonTerminal.PARAMETER,	new Symbol[]{Terminal.COMMA, NonTerminal.TYPE, NonTerminal.IDENTIFIER, Action.addParameter, NonTerminal.PARAMETER, }),
    R21(NonTerminal.PARAMETER,	new Symbol[]{}),
    R22(NonTerminal.TYPE,	new Symbol[]{Action.pushNext, Terminal.BOOLEAN, }),
    R23(NonTerminal.TYPE,	new Symbol[]{Action.pushNext, Terminal.INT, }),
    R24(NonTerminal.STATEMENTS,	new Symbol[]{NonTerminal.STATEMENT, NonTerminal.STATEMENTS, }),
    R25(NonTerminal.STATEMENTS,	new Symbol[]{}),
    R26(NonTerminal.STATEMENT,	new Symbol[]{Terminal.LEFT_BRACE, NonTerminal.STATEMENTS, Terminal.RIGHT_BRACE, }),
    R27(NonTerminal.STATEMENT,	new Symbol[]{Terminal.IF, Terminal.LEFT_PAREN, NonTerminal.GEN_EXPRESSION, Terminal.RIGHT_PAREN, Action.save, NonTerminal.STATEMENT, Action.save, Terminal.ELSE, NonTerminal.STATEMENT, Action.endIf, }),
    R28(NonTerminal.STATEMENT,	new Symbol[]{Action.label, Terminal.WHILE, Terminal.LEFT_PAREN, NonTerminal.GEN_EXPRESSION, Terminal.RIGHT_PAREN, Action.save, NonTerminal.STATEMENT, Action.endWhile, }),
    R29(NonTerminal.STATEMENT,	new Symbol[]{Action.jumpNext, Action.save, Terminal.SWITCH, Terminal.LEFT_PAREN, NonTerminal.GEN_EXPRESSION, Terminal.RIGHT_PAREN, Terminal.LEFT_BRACE, NonTerminal.CASE_STATEMENTS, Terminal.RIGHT_BRACE, Action.endSwitch, }),
    R30(NonTerminal.STATEMENT,	new Symbol[]{Terminal.SYSTEM_OUT, Terminal.LEFT_PAREN, NonTerminal.GEN_EXPRESSION, Terminal.RIGHT_PAREN, Terminal.SEMICOLON, Action.print, }),
    R31(NonTerminal.STATEMENT,	new Symbol[]{NonTerminal.IDENTIFIER, Action.varRef, Terminal.ASSIGN, NonTerminal.GEN_EXPRESSION, Terminal.SEMICOLON, Action.assign, }),
    R32(NonTerminal.CASE_STATEMENTS,	new Symbol[]{NonTerminal.CASE_STATEMENT, NonTerminal.CASE_STATEMENTS_REM, }),
    R33(NonTerminal.CASE_STATEMENTS_REM,	new Symbol[]{NonTerminal.CASE_STATEMENT, NonTerminal.CASE_STATEMENTS_REM, }),
    R34(NonTerminal.CASE_STATEMENTS_REM,	new Symbol[]{}),
    R35(NonTerminal.CASE_STATEMENT,	new Symbol[]{Terminal.CASE, Action.dup, NonTerminal.GEN_EXPRESSION, Action.equals, Action.save, Terminal.COLON, NonTerminal.STATEMENTS, Terminal.BREAK, Terminal.SEMICOLON, Action.endCase, }),
    R36(NonTerminal.GEN_EXPRESSION,	new Symbol[]{NonTerminal.EXPRESSION, NonTerminal.GEN_EXPRESSION_REM, }),
    R37(NonTerminal.GEN_EXPRESSION_REM,	new Symbol[]{}),
    R38(NonTerminal.GEN_EXPRESSION_REM,	new Symbol[]{NonTerminal.REL_EXPRESSION_REM_REM, }),
    R39(NonTerminal.EXPRESSION,	new Symbol[]{NonTerminal.TERM, NonTerminal.EXPRESSION_REM, }),
    R40(NonTerminal.EXPRESSION_REM,	new Symbol[]{Terminal.PLUS, NonTerminal.TERM, Action.add, NonTerminal.EXPRESSION_REM, }),
    R41(NonTerminal.EXPRESSION_REM,	new Symbol[]{Terminal.MINUS, NonTerminal.TERM, Action.minus, NonTerminal.EXPRESSION_REM, }),
    R42(NonTerminal.EXPRESSION_REM,	new Symbol[]{}),
    R43(NonTerminal.TERM,	new Symbol[]{NonTerminal.FACTOR, NonTerminal.TERM_REM, }),
    R44(NonTerminal.TERM_REM,	new Symbol[]{}),
    R45(NonTerminal.TERM_REM,	new Symbol[]{Terminal.TIMES, NonTerminal.TERM, Action.mult, }),
    R46(NonTerminal.FACTOR,	new Symbol[]{Terminal.LEFT_PAREN, NonTerminal.EXPRESSION, Terminal.RIGHT_PAREN, }),
    R47(NonTerminal.FACTOR,	new Symbol[]{NonTerminal.IDENTIFIER, NonTerminal.FACTOR_REM, }),
    R48(NonTerminal.FACTOR_REM,	new Symbol[]{Action.varRef}),
    R49(NonTerminal.FACTOR_REM,	new Symbol[]{Terminal.DOT, NonTerminal.IDENTIFIER, NonTerminal.FACTOR_REM_REM, }),
    R50(NonTerminal.FACTOR_REM_REM,	new Symbol[]{Action.fieldRef}),
    R51(NonTerminal.FACTOR_REM_REM,	new Symbol[]{Action.methodRef, Terminal.LEFT_PAREN, NonTerminal.ARGUMENTS, Terminal.RIGHT_PAREN, Action.methodCall, }),
    R52(NonTerminal.FACTOR,	new Symbol[]{Terminal.TRUE, Action.pushTrue, }),
    R53(NonTerminal.FACTOR,	new Symbol[]{Terminal.FALSE, Action.pushFalse, }),
    R54(NonTerminal.FACTOR,	new Symbol[]{NonTerminal.INTEGER, }),
    R55(NonTerminal.REL_EXPRESSION_REM,	new Symbol[]{Terminal.AND_AND, NonTerminal.EXPRESSION, Action.and, NonTerminal.REL_EXPRESSION_REM_REM, }),
    R56(NonTerminal.REL_EXPRESSION_REM_REM,	new Symbol[]{Terminal.EQUALS, NonTerminal.EXPRESSION, Action.equals, NonTerminal.REL_EXPRESSION_REM, }),
    R57(NonTerminal.REL_EXPRESSION_REM_REM,	new Symbol[]{Terminal.LESS_THAN, NonTerminal.EXPRESSION, Action.lessThan, NonTerminal.REL_EXPRESSION_REM, }),
    R58(NonTerminal.REL_EXPRESSION_REM,	new Symbol[]{}),
    R59(NonTerminal.ARGUMENTS,	new Symbol[]{NonTerminal.GEN_EXPRESSION, Action.methodArg, NonTerminal.ARGUMENT, }),
    R60(NonTerminal.ARGUMENTS,	new Symbol[]{}),
    R61(NonTerminal.ARGUMENT,	new Symbol[]{Terminal.COMMA, NonTerminal.GEN_EXPRESSION, Action.methodArg, NonTerminal.ARGUMENT, }),
    R62(NonTerminal.ARGUMENT,	new Symbol[]{}),
    R63(NonTerminal.IDENTIFIER,	new Symbol[]{Action.pushNext, Terminal.IDENTIFIER_LITERAL, }),
    R64(NonTerminal.INTEGER,	new Symbol[]{Action.pushInteger, Terminal.INTEGER_LITERAL, }),
    ;

    private NonTerminal lhs;
    private Symbol[] rhs;

    private Rule(NonTerminal lhs, Symbol[] rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public NonTerminal getLeftHandSide() {
        return lhs;
    }

    public Symbol[] getRightHandSide() {
        return rhs;
    }

    @Override
    public String toString() {
        String s = "";
        for (Symbol r : rhs)
            s += r.toString() + " ";
        return String.format("( %s: %s -> %s)", name(), lhs.toString(), s);
    }
}
