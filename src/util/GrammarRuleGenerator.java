package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Copyright (C) 2016 Hadi
 */
public class GrammarRuleGenerator {
    private static final HashMap<String, String> specials = new HashMap<>();

    public static void main(String[] args) {
        System.out.println("Please enter the grammar rules:");
        ArrayList<String> nonterminals = new ArrayList<>();
        ArrayList<String> terminals = new ArrayList<>();
        ArrayList<String> rules = new ArrayList<>();
        Scanner s = new Scanner(System.in);
        while (s.hasNext()) {
            System.out.println(convert(s.nextLine()).toUpperCase());
        }
        while (s.hasNext()) {
            String line = s.nextLine();
            String[] split = line.split("\\s*->\\s*");
            String lhs = split[0];
            String rhs[] = split[1].split("\\s+");
            for (int i = 0; i < rhs.length; i++) {
                rhs[i] = convert(rhs[i]);
            }
        }
//        for (Rule r : rules) {
//            System.out.println();
//        }
    }

    private static String convert(String name) {
        return name.replaceAll("(.)([A-Z])", "$1_$2");
    }

    private class Rule {
        public final String lhs;
        public final String[] rhs;

        public Rule(String lhs, String[] rhs) {
            this.lhs = lhs;
            this.rhs = rhs;
        }
    }
}
