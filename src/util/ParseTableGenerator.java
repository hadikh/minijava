package util;

import lang.NonTerminal;
import lang.Symbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Copyright (C) 2016 Hadi
 */
public class ParseTableGenerator {
    public static void main(String[] args) {
        ArrayList<String> terminals = new ArrayList<>();
        ArrayList<String> nonterminals = new ArrayList<>();
        ArrayList<Rule> rules = new ArrayList<>();
        Scanner s = new Scanner(System.in);
        while (s.hasNext()) {
            String line = s.nextLine();
            String[] split = line.split("\\s*->\\s*");
            String lhs = split[0].trim();
            String rhs[] = split.length == 1 ? new String[] {} : split[1].trim().split("\\s+");
            rules.add(new Rule(lhs, rhs));
        }

        for (Rule r : rules) {
            if (!nonterminals.contains(r.lhs))
                nonterminals.add(r.lhs);
        }
        for (Rule r : rules) {
            for (String t : r.rhs) {
                if (!nonterminals.contains(t) && !terminals.contains(t))
                    terminals.add(t);
            }
        }

        int a = 1;
        for (Rule r : rules) {
            System.out.printf("R%d(NonTerminal.%s,\tnew Symbol[]{", a++, r.lhs);
            for (String rs : r.rhs) {
                if (nonterminals.contains(rs)) {
                    System.out.printf("NonTerminal.%s, ", rs);
                } else {
                    System.out.printf("Terminal.%s, ", rs);
                }
            }
            System.out.printf("}),\n");
        }

        HashMap<String, HashSet<String>> first = new HashMap<>();
        for (String t : terminals) {
            HashSet<String> arr = new HashSet<>();
            arr.add(t);
            first.put(t, arr);
        }
        for (String nt : nonterminals) {
            first.put(nt, new HashSet<>());
        }
        boolean changed;
        do {
            changed = false;
            for (Rule rule : rules) {
                HashSet<String> newFirst = new HashSet<>(first.get(rule.lhs));
                boolean eps = newFirst.contains("EPSILON");
                newFirst.add("EPSILON");
                for (String r : rule.rhs) {
                    changed |= newFirst.addAll(first.get(r));
                    if (!first.get(r).contains("EPSILON")) {
                        eps = false;
                        break;
                    }
                }
                if (!eps)
                    newFirst.remove("EPSILON");
            }
        } while (changed);

        HashMap<String, ArrayList<String>> follow = new HashMap<>();
//        for (NonTerminal nt : nonterminals) {
//
//        }

    }

    private static class Rule {
        public final String lhs;
        public final String[] rhs;

        public Rule(String lhs, String[] rhs) {
            this.lhs = lhs;
            this.rhs = rhs;
        }
    }
}
