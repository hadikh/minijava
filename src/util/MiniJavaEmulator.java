package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Consumer;

/**
 * Copyright (C) 2016 Hadi
 */
public class MiniJavaEmulator {
    private final boolean logFlag = false;
    private String[] commands;
    private static int ip;
    private static HashMap<Integer, Integer> memory = new HashMap<>();

    private void start() {
        readAllCommands();
        while (ip < commands.length) {
            runCommand();
            ip++;
        }
    }

    private void readAllCommands() {
        ArrayList<String> commandsList = new ArrayList<>();
        Scanner s = new Scanner(System.in);
        while (s.hasNext()) {
            String line = s.nextLine();
            if (line.equals("EOF"))
                break;
            commandsList.add(line);
        }
        commands = commandsList.toArray(new String[commandsList.size()]);
    }

    private void runCommand() {
        String line = commands[ip].trim();
        if (logFlag)
            System.err.println("Running " + line);
        String args[] = line.substring(1, line.length() - 1).split("\\s*,\\s*");
        Command cmd = Command.valueOf(args[0]);
        cmd.run(args);
    }

    public static void main(String[] args) {
        new MiniJavaEmulator().start();
    }

    public static int collectValue(String arg) {
        if (arg.charAt(0) == '#')
            return Integer.parseInt(arg.substring(1));
        if (arg.charAt(0) == '@')
            return memory.getOrDefault(memory.getOrDefault(Integer.parseInt(arg.substring(1)), 0), 0);
        return memory.getOrDefault(Integer.parseInt(arg), 0);
    }

    public static int collectAddress(String arg) {
        if (arg.charAt(0) == '#')
            throw new RuntimeException("Address could not start with #.");
        if (arg.charAt(0) == '@')
            return memory.getOrDefault(Integer.parseInt(arg.substring(1)), 0);
        return Integer.parseInt(arg);
    }

    enum Command {
        ADD(args -> {
            memory.put(collectAddress(args[3]), collectValue(args[1]) + collectValue(args[2]));
        }),
        AND(args -> {
            memory.put(collectAddress(args[3]), collectValue(args[1]) & collectValue(args[2]));
        }),
        ASSIGN(args -> {
            memory.put(collectAddress(args[2]), collectValue(args[1]));
        }),
        EQ(args -> {
            memory.put(collectAddress(args[3]), collectValue(args[1]) == collectValue(args[2]) ? 1 : 0);
        }),
        JPF(args -> {
            if (collectValue(args[1]) == 0)
                ip = collectAddress(args[2]) - 1;
        }),
        JP(args -> {
            ip = collectAddress(args[1]) - 1;
        }),
        LT(args -> {
            memory.put(collectAddress(args[3]), collectValue(args[1]) < collectValue(args[2]) ? 1 : 0);
        }),
        MULT(args -> {
            memory.put(collectAddress(args[3]), collectValue(args[1]) * collectValue(args[2]));
        }),
        NOT(args -> {
            memory.put(collectAddress(args[2]), collectValue(args[1]) == 0 ? 1 : 0);
        }),
        PRINT(args -> {
            System.out.println(memory.get(collectAddress(args[1])));
        }),
        SUB(args -> {
            memory.put(collectAddress(args[3]), collectValue(args[1]) - collectValue(args[2]));
        }),
        ;

        private Consumer<String[]> action;

        private Command(Consumer<String[]> action) {
            this.action = action;
        }

        public void run(String[] args) {
            action.accept(args);
        }
    }
}
