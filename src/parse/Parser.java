package parse;

import codegeneration.Action;
import codegeneration.CodeGenerator;
import lang.*;
import scan.Scanner;
import scan.Token;
import codegeneration.Instruction;

import java.util.Stack;

/**
 * LL1 parser of the grammar
 */
public class Parser {
    private Scanner scanner;

    public Parser(Scanner scanner) {
        this.scanner = scanner;
    }

    public Parser() {
        this(new Scanner());
    }

    public static void main(String[] args) {
        Parser p = new Parser();
        p.parse();
    }

    public void parse() {
        Stack<Token> parseStack = new Stack<>();
        parseStack.push(new Token(NonTerminal.GOAL));
        Token x, a = scanner.getNextToken();
        CodeGenerator g = new CodeGenerator();
        Symbol xSymbol;
        do {
            x = parseStack.peek();
            xSymbol = x.getSymbol();
            if (xSymbol instanceof Action) {
                parseStack.pop();
                g.lookAhead = a;
                try {
                    ((Action) xSymbol).run(g);
                } catch (Exception ignore) {
                }
            } else if (xSymbol instanceof Terminal) {
                if (xSymbol.equals(a.getSymbol())) {
                    parseStack.pop();
                    a = scanner.getNextToken();
                } else {
                    err("Missing token '" + ((Terminal) xSymbol).getKeyword() + "' inserted at line " + a.getLineNumber() + " column " + a.getColumnNumber() + ".");
                    parseStack.pop();
                }
            } else {
                Rule r = ParseTable.at((NonTerminal) xSymbol, (Terminal) a.getSymbol());
                if (r == null) {
                    // no rule
                    err("Wrong token '" + a.getValue() + "' removed from line " + a.getLineNumber() + " column " + a.getColumnNumber() + ".");
                    a = scanner.getNextToken();
                } else if (r == Rule.R0) {
                    // sync
                    parseStack.pop();
                    err(((NonTerminal) xSymbol).getMissingError() + ", token '" + a.getValue() + "' on line " + a.getLineNumber() + " column " + a.getColumnNumber() + ".");
                } else {
                    x.setLineNumber(a.getLineNumber());
                    x.setColumnNumber(a.getColumnNumber());
                    parseStack.pop();
                    Symbol[] rhs = r.getRightHandSide();
                    for (int i = 0; i < rhs.length; i++)
                        parseStack.push(new Token(rhs[rhs.length - i - 1]));
                }
            }
        } while (!parseStack.empty() && (a.getSymbol() != Terminal.EOF || !"$".equals(a.getValue())));

        if (!parseStack.empty()) {
            err("End of file reached while expecting more input.");
        } else if (a.getSymbol() != Terminal.EOF || !"$".equals(a.getValue())) {
            err("Expected end of file but found '" + a.getValue() + "'.");
        }

        for (Instruction ins : g.programBlock.getAll()) {
            System.out.println(ins);
        }
    }

    private void err(String msg) {
        System.err.println("Parse Error: " + msg);
    }
}
