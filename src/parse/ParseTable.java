package parse;

import lang.NonTerminal;
import lang.Rule;
import lang.Terminal;

/**
 * LL1 parse table of the grammar
 */
public class ParseTable {
        private static final Rule[][] table = {
                {Rule.R1, Rule.R1, Rule.R1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {Rule.R0, Rule.R2, Rule.R2, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {Rule.R0, Rule.R3, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, Rule.R5, Rule.R4, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, Rule.R0, Rule.R6, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, Rule.R8, null, null, null, null, null, null, Rule.R7, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, Rule.R10, null, null, Rule.R9, null, null, null, null, Rule.R10, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, Rule.R0, null, null, Rule.R11, null, null, null, null, Rule.R0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, Rule.R13, null, null, null, null, null, Rule.R13, null, null, Rule.R13, null, Rule.R12, Rule.R12, Rule.R13, null, Rule.R13, Rule.R13, Rule.R13, null, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R13, null},
                {null, null, null, Rule.R0, null, null, null, null, null, Rule.R0, null, null, Rule.R0, null, Rule.R14, Rule.R14, Rule.R0, null, Rule.R0, Rule.R0, Rule.R0, null, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R0, null},
                {null, Rule.R15, null, null, null, null, null, null, null, Rule.R16, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, Rule.R17, null, null, null, null, null, null, null, Rule.R0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, Rule.R19, null, null, null, null, null, Rule.R18, Rule.R18, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, Rule.R21, null, null, null, null, Rule.R20, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R22, Rule.R23, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R0, null},
                {null, null, null, Rule.R24, null, null, null, null, null, Rule.R25, null, null, Rule.R25, null, null, null, Rule.R24, null, Rule.R24, Rule.R24, Rule.R24, null, null, null, Rule.R25, null, null, null, null, null, null, null, null, null, Rule.R24, null},
                {null, null, null, Rule.R26, null, null, null, null, null, Rule.R0, null, null, Rule.R0, null, null, null, Rule.R27, Rule.R0, Rule.R28, Rule.R29, Rule.R30, null, null, null, Rule.R0, null, null, null, null, null, null, null, null, null, Rule.R31, null},
                {null, null, null, null, null, null, null, null, null, Rule.R0, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R32, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, Rule.R34, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R33, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, Rule.R0, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R35, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, Rule.R36, Rule.R0, null, null, Rule.R0, null, Rule.R0, null, null, null, null, null, null, null, null, null, Rule.R0, null, null, null, null, null, Rule.R36, Rule.R36, null, null, null, Rule.R36, Rule.R36},
                {null, null, null, null, null, null, null, null, Rule.R37, null, null, Rule.R37, null, Rule.R37, null, null, null, null, null, null, null, null, null, Rule.R37, null, null, null, null, null, null, null, null, Rule.R38, Rule.R38, null, null},
                {null, null, null, null, null, null, null, Rule.R39, Rule.R0, null, null, Rule.R0, null, Rule.R0, null, null, null, null, null, null, null, null, null, Rule.R0, null, null, null, null, null, Rule.R39, Rule.R39, Rule.R0, Rule.R0, Rule.R0, Rule.R39, Rule.R39},
                {null, null, null, null, null, null, null, null, Rule.R42, null, null, Rule.R42, null, Rule.R42, null, null, null, null, null, null, null, null, null, Rule.R42, null, Rule.R40, Rule.R41, null, null, null, null, Rule.R42, Rule.R42, Rule.R42, null, null},
                {null, null, null, null, null, null, null, Rule.R43, Rule.R0, null, null, Rule.R0, null, Rule.R0, null, null, null, null, null, null, null, null, null, Rule.R0, null, Rule.R0, Rule.R0, null, null, Rule.R43, Rule.R43, Rule.R0, Rule.R0, Rule.R0, Rule.R43, Rule.R43},
                {null, null, null, null, null, null, null, null, Rule.R44, null, null, Rule.R44, null, Rule.R44, null, null, null, null, null, null, null, null, null, Rule.R44, null, Rule.R44, Rule.R44, Rule.R45, null, null, null, Rule.R44, Rule.R44, Rule.R44, null, null},
                {null, null, null, null, null, null, null, Rule.R46, Rule.R0, null, null, Rule.R0, null, Rule.R0, null, null, null, null, null, null, null, null, null, Rule.R0, null, Rule.R0, Rule.R0, Rule.R0, null, Rule.R52, Rule.R53, Rule.R0, Rule.R0, Rule.R0, Rule.R47, Rule.R54},
                {null, null, null, null, null, null, null, null, Rule.R48, null, null, Rule.R48, null, Rule.R48, null, null, null, null, null, null, null, null, null, Rule.R48, null, Rule.R48, Rule.R48, Rule.R48, Rule.R49, null, null, Rule.R48, Rule.R48, Rule.R48, null, null},
                {null, null, null, null, null, null, null, Rule.R51, Rule.R50, null, null, Rule.R50, null, Rule.R50, null, null, null, null, null, null, null, null, null, Rule.R50, null, Rule.R50, Rule.R50, Rule.R50, null, null, null, Rule.R50, Rule.R50, Rule.R50, null, null},
                {null, null, null, null, null, null, null, null, Rule.R58, null, null, Rule.R58, null, Rule.R58, null, null, null, null, null, null, null, null, null, Rule.R58, null, null, null, null, null, null, null, Rule.R55, null, null, null, null},
                {null, null, null, null, null, null, null, null, Rule.R0, null, null, Rule.R0, null, Rule.R0, null, null, null, null, null, null, null, null, null, Rule.R0, null, null, null, null, null, null, null, null, Rule.R56, Rule.R57, null, null},
                {null, null, null, null, null, null, null, Rule.R59, Rule.R60, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, Rule.R59, Rule.R59, null, null, null, Rule.R59, Rule.R59},
                {null, null, null, null, null, null, null, null, Rule.R62, null, null, null, null, Rule.R61, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, Rule.R0, null, null, null, Rule.R0, Rule.R0, null, Rule.R0, Rule.R0, null, Rule.R0, null, null, null, null, null, null, null, Rule.R0, null, Rule.R0, null, Rule.R0, Rule.R0, Rule.R0, Rule.R0, null, null, Rule.R0, Rule.R0, Rule.R0, Rule.R63, null},
                {null, null, null, null, null, null, null, null, Rule.R0, null, null, Rule.R0, null, Rule.R0, null, null, null, null, null, null, null, null, null, Rule.R0, null, Rule.R0, Rule.R0, Rule.R0, null, null, null, Rule.R0, Rule.R0, Rule.R0, null, Rule.R64},
        };

        public static Rule at(NonTerminal nonTerminal, Terminal terminal) {
                return table[nonTerminal.ordinal()][terminal.ordinal()];
        }
}
