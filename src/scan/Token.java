package scan;

import lang.Symbol;

/**
 * Token, which is returned by scanner
 */
public class Token {
    private Symbol symbol;
    private String value;
    private int lineNumber;
    private int columnNumber;

    public Token(Symbol symbol, String value, int lineNumber, int columnNumber) {
        this.symbol = symbol;
        this.value = value;
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
    }

    public Token(Symbol symbol) {
        this(symbol, null, -1, -1);
    }

    public Symbol getSymbol() {
        return this.symbol;
    }

    public String getValue() {
        return this.value;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(int columnNumber) {
        this.columnNumber = columnNumber;
    }

    @Override
    public String toString() {
        return "Token(" + symbol + "," + value + ")";
    }
}
