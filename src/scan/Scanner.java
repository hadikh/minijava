package scan;

import lang.Terminal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Scanner, reads and returns tokens to the parser
 */
public class Scanner {
    private BufferedReader reader;
    private Map<String, Terminal> terminalMap;
    private int columnNumber = 0;
    private int lineNumber = 1;
    private int lastTokenLength = 0;
    private char tokenBuffer[];
    private int current = 0;

    public Scanner(InputStream is, int bufferSize) {
        reader = new BufferedReader(new InputStreamReader(is), bufferSize);
        tokenBuffer = new char[bufferSize];

        terminalMap = new HashMap<>();
        for (Terminal t : Terminal.values()) {
            terminalMap.put(t.getKeyword(), t);
        }
    }

    public Scanner() {
        this(System.in, 2048);
    }

    private String next() throws IOException {
        columnNumber += lastTokenLength;
        if (current == 0)
            current = reader.read();
        for (; ; ) {
            if (current == -1) {
                throw new EOFReachException();
            }
            if (current == ' ' || current == '\t') {
                columnNumber++;
                current = reader.read();
                continue;
            }
            if (current == '\n') {
                lineNumber++;
                columnNumber = 0;
                current = reader.read();
                continue;
            }
            break;
        }
        int tokenLength = 0;
        while (current != ' ' && current != '\n' && current != '\t' && current != -1) {
            if (tokenLength == tokenBuffer.length) {
                char newTokenBuffer[] = new char[2 * tokenLength];
                System.arraycopy(tokenBuffer, 0, newTokenBuffer, 0, tokenLength);
                tokenBuffer = newTokenBuffer;
            }
            tokenBuffer[tokenLength++] = (char) current;
            current = reader.read();
        }
        lastTokenLength = tokenLength;
        return new String(tokenBuffer, 0, tokenLength);
    }

    private void skipLine() throws IOException {
        if (current == 0)
            current = reader.read();
        for (; ; ) {
            if (current == -1) {
                throw new EOFReachException();
            }
            if (current == '\n') {
                lineNumber++;
                columnNumber = 0;
                current = reader.read();
                break;
            }
            current = reader.read();
        }
    }

    public Token getNextTokenUnsafe() throws IOException {
        String token = next();

        // skip single line comment
        if (token.startsWith("//")) {
            skipLine();
            return getNextToken();
        }

        // skip multiline comment
        if (token.startsWith("/*")) {
            token = token.substring(2);
            while (!token.endsWith("*/"))
                token = next();
            return getNextTokenUnsafe();
        }

        // check for terminals
        if (terminalMap.containsKey(token))
            return new Token(terminalMap.get(token), token, lineNumber, columnNumber);

        // check for identifier pattern
        if (token.matches("[A-Za-z][A-Za-z0-9]*"))
            return new Token(Terminal.IDENTIFIER_LITERAL, token, lineNumber, columnNumber);

        // check for number
        if (token.matches("(\\+|\\-)?\\d+"))
            return new Token(Terminal.INTEGER_LITERAL, token, lineNumber, columnNumber);

        System.err.println("Error: Unknown token " + token + ".");

        return getNextToken();
    }

    public Token getNextToken() {
        try {
            return getNextTokenUnsafe();
        } catch (EOFReachException ignore) {
            return new Token(Terminal.EOF, "$", lineNumber, columnNumber);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println("Testing Scanner...");
        System.out.println("Enter input and check corresponding tokens to ensure that scanner works correctly.");
        Scanner s = new Scanner();
        while (true) {
            System.out.println(s.getNextToken());
        }
    }

}
